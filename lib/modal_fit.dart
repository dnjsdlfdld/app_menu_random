import 'package:flutter/material.dart';

class ModalFit extends StatelessWidget {
  const ModalFit({super.key, required this.callback});

  final VoidCallback callback;

  @override
  Widget build(BuildContext context) {
    return Material(
      child: SafeArea(
        top: false,
        child: Column(
          mainAxisSize: MainAxisSize.min,
          children: [
            ListTile(
              title: Text('삭제'),
              leading: Icon(Icons.delete),
              onTap: callback,
            )
          ],
        ),
      ),
    );
  }
}
