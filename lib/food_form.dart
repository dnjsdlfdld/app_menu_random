import 'package:flutter/material.dart'; // 디자인이 들어간 곳에서는 무조건 다 들어가야 됨.
import 'package:launch_menu/modal_fit.dart';
import 'dart:math';

import 'package:modal_bottom_sheet/modal_bottom_sheet.dart';

class FoodForm extends StatefulWidget {
  const FoodForm({super.key, required this.title}); // 생성자

  final String title;


  @override
  State<FoodForm> createState() => _FoodFormState();
}

class _FoodFormState extends State<FoodForm> {
  final _scrollController = ScrollController();
  
  var _foodList = [];

  String _foodResult = "";
  String _inputFoodName = "";

  void getFood() { // 준비해 놓은 배열이 있기에 받을 값은 없다.
    int resultIndex = Random().nextInt(_foodList.length);

    setState(() {
      _foodResult = "오늘 점심 메뉴는 " + _foodList[resultIndex] + "어때?";
    });
  }

  void addFood() { // 추가하기를 눌렀을 때 실행되는 메소드
    setState(() {
      _foodList.add(_inputFoodName);
    });
  }

  void delFood(int index) {
    setState(() {
      _foodList.removeAt(index);
    });
  }
  
  Widget _buildBody() {
    return Column(
      mainAxisSize: MainAxisSize.max,
      mainAxisAlignment: MainAxisAlignment.start,
      crossAxisAlignment: CrossAxisAlignment.stretch,
      children: [
        Container(
          padding: const EdgeInsets.fromLTRB(100, 20, 100, 20),
          child: OutlinedButton
            (onPressed: () {
            getFood();
          },
            child: Text("점심 골라줘(클릭)"),
          ),
        ),
        Container(
          margin: const EdgeInsets.fromLTRB(100, 0, 100, 20),
          alignment: Alignment.center,
          color: Color.fromRGBO(199, 168, 240, 80), // opacity 투명도
          child: Text(_foodResult),
        ),
        ListView.builder(
          physics: NeverScrollableScrollPhysics(),
          shrinkWrap: true, // ***
          itemCount: _foodList.length,
          // foodList 의 길이만큼 돌겠다
          itemBuilder: (_, index) => _buildListItem(index, _foodList[index]),
        ),
      ],
    );
  }

  Widget _buildListItem(int idx, String foodName) { // 삭제하려면 몇 번째 놈인지 알아야 한다.
    return ListTile(
      leading: CircleAvatar(
        child: Text(' ${idx + 1}'), // 보여줄때만 +1 된다
      ),
      title: Text('$foodName'),
      trailing: const Icon(Icons.delete, color: Colors.red, size: 27),
      onTap: () => showMaterialModalBottomSheet(
          expand: false,
        context: context,
        backgroundColor: Colors.transparent,
        builder: (context) => ModalFit(
          callback: () {
            delFood(idx);
            Navigator.of(context).pop();
          },
        ),
      ),
    );
  }

  void _showDialog() {
    showDialog(
        context: context,
        barrierDismissible: false, // 바깥쪽을 누르면 꺼지게 할 것이냐
        builder: (BuildContext context) {
          return AlertDialog(
            title: Text("음식 추가"),
            content: Container(
              child: TextField(

                decoration: InputDecoration(
                  labelText: "음식명을 적어주세요",
                  border: const OutlineInputBorder(
                    borderRadius: BorderRadius.all(Radius.circular(15.0)) // 둥글게
                  ),
                ),
                onChanged: (val) => _inputFoodName = val,
              ),
            ),
            
            actions: [  // 이 영역에 버튼이 들어간다

              OutlinedButton(
                  onPressed: () {
                    addFood();
                      Navigator.of(context).pop();
                  },
                  child: Text("추가하기")),

              OutlinedButton(
                  onPressed: () {
                    Navigator.of(context).pop();
                  },
                  child: Text("꺼라"))

            ],
          );
        }
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(widget.title),
      ),
      body: ListView(
        controller: _scrollController, //  스크롤을 누구한테 하라고 지시
        children: [
          _buildBody(), // 너무 길기 때문에 한 뭉탱이 빼준거임
        ],
      ),
      floatingActionButton: FloatingActionButton(
        child: const Icon(Icons.adb_rounded),
        onPressed: () {
          _showDialog();
        },
      ),
    );
  }
}


