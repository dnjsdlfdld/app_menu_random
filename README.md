# 점심 랜덤추천 APP

***
### LANGUAGE
```
* Dart
* Flutter
```
***
### 기능
```
    - 데이터 추가 기능
    - 데이터 삭제 기능
    - List에 있는 데이터로 Random 함수 적용
    - Random으로 뽑힌 데이터 Text 출력
```
![covid_clinic](./assets/random.png)